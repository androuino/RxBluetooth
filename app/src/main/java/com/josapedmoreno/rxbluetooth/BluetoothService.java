package com.josapedmoreno.rxbluetooth;

import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.github.ivbaranov.rxbluetooth.RxBluetooth;
import com.github.ivbaranov.rxbluetooth.events.AclEvent;
import com.github.ivbaranov.rxbluetooth.events.ConnectionStateEvent;
import com.github.ivbaranov.rxbluetooth.events.ServiceEvent;
import com.github.ivbaranov.rxbluetooth.predicates.BtPredicate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by josapedmoreno on 2017/10/05.
 */

public class BluetoothService extends Service {
    private static final String TAG = "BluetoothService";
    private static final int REQUEST_ENABLE_BT = 1;
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private RxBluetooth rxBluetooth;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private BluetoothDevice mBluetoothDevice;
    private List<BluetoothDevice> devices = new ArrayList<>();
    private Boolean isConnected = false;
    private AutoConnect autoConnect;

    @Override public void onCreate() {
        super.onCreate();
        autoConnect = new AutoConnect();

        Log.d(TAG, "BluetoothService started!");
        rxBluetooth = new RxBluetooth(this);

        if (!rxBluetooth.isBluetoothAvailable()) {
            // handle the lack of bluetooth support
            Log.d(TAG, "Bluetooth is not supported!");
        } else {
            // check if bluetooth is currently enabled and ready for use
            if (!rxBluetooth.isBluetoothEnabled()) {
                Log.d(TAG, "Bluetooth should be enabled first!");
            } else {
                // check if bluetooth is currently enabled and ready for use
                if (!rxBluetooth.isBluetoothEnabled()) {
                    // to enable bluetooth via startActivityForResult()
                    Log.d(TAG, "Enabling Bluetooth");
                    rxBluetooth.enableBluetooth((Activity) getApplicationContext(), REQUEST_ENABLE_BT);
                } else {
                    // you are ready
                    compositeDisposable.add(rxBluetooth.observeDevices()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Consumer<BluetoothDevice>() {
                                @Override
                                public void accept(@NonNull BluetoothDevice bluetoothDevice) throws Exception {
                                    addDevice(bluetoothDevice);
                                    mBluetoothDevice = bluetoothDevice;
                                }
                            }));

                    compositeDisposable.add(rxBluetooth.observeDiscovery()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .filter(BtPredicate.in(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
                            .subscribe(new Consumer<String>() {
                                @Override
                                public void accept(String action) throws Exception {
                                }
                            }));

                    compositeDisposable.add(rxBluetooth.observeDiscovery()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .filter(BtPredicate.in(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
                            .subscribe(new Consumer<String>() {
                                @Override
                                public void accept(String action) throws Exception {
                                }
                            }));

                    compositeDisposable.add(rxBluetooth.observeBluetoothState()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .filter(BtPredicate.in(BluetoothAdapter.STATE_ON))
                            .subscribe(new Consumer<Integer>() {
                                @Override
                                public void accept(Integer integer) throws Exception {
                                }
                            }));

                    compositeDisposable.add(rxBluetooth.observeBluetoothState()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .filter(BtPredicate.in(BluetoothAdapter.STATE_OFF,
                                    BluetoothAdapter.STATE_TURNING_OFF, BluetoothAdapter.STATE_TURNING_ON))
                            .subscribe(new Consumer<Integer>() {
                                @Override
                                public void accept(Integer integer) {
                                }
                            }));
                    compositeDisposable.add(rxBluetooth.observeConnectionState()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .subscribe(new Consumer<ConnectionStateEvent>() {
                                @Override
                                public void accept(ConnectionStateEvent event) throws Exception {
                                    switch (event.getState()) {
                                        case BluetoothAdapter.STATE_DISCONNECTED:
                                            // device disconnected
                                            Log.d(TAG, "Disconnected");
                                            break;
                                        case BluetoothAdapter.STATE_CONNECTING:
                                            // device connecting
                                            break;
                                        case BluetoothAdapter.STATE_CONNECTED:
                                            // device connected
                                            Log.d(TAG, "Connected");
                                            break;
                                        case BluetoothAdapter.STATE_DISCONNECTING:
                                            // device disconnecting
                                            break;
                                    }
                                }
                            }));
                    compositeDisposable.add(rxBluetooth.observeAclEvent()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.computation())
                            .subscribe(new Consumer<AclEvent>() {
                                @Override
                                public void accept(AclEvent aclEvent) throws Exception {
                                    switch (aclEvent.getAction()) {
                                        case BluetoothDevice.ACTION_ACL_CONNECTED:
                                            Log.d(TAG, "Connected");
                                            isConnected = true;
                                            break;
                                        case BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED:
                                            //...
                                            break;
                                        case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                                            Log.d(TAG, "Disconnected");
                                            isConnected = false;
                                            try {
                                                autoConnect.start();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                    }
                                }
                            }));
                    rxBluetooth.startDiscovery();
                }
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void addDevice(BluetoothDevice device) {
        devices.add(device);
        setAdapter(devices);
    }

    private void setAdapter(List<BluetoothDevice> list) {
        BluetoothDevice device = null;
        for (int i = 0; i < list.size(); i++) {
            device = list.get(i);
        }
        String devName = device.getName();
        String devAddress = device.getAddress();
        Log.d(TAG, devName + " - " + devAddress);
        rxBluetooth.observeConnectDevice(device, uuid)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<BluetoothSocket>() {
                    @Override
                    public void accept(@NonNull BluetoothSocket bluetoothSocket) throws Exception {
                        Log.d(TAG, "Success " + bluetoothSocket.toString());
                        try {
                            autoConnect.isInterrupted();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                    }
                });

        if (TextUtils.isEmpty(devName)) {
            devName = "NO NAME";
        }
    }

    private class AutoConnect extends Thread {
        @Override
        public void run() {
            Log.d(TAG, "AutoConnect started!");
            try {
                while (!isConnected) {
                    addDevice(mBluetoothDevice);
                    Thread.sleep(3000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.run();
        }

        @Override
        public boolean isInterrupted() {
            Log.d(TAG, "AutoConnect stopped!");
            return super.isInterrupted();
        }

        @Override
        public void interrupt() {
            super.interrupt();
        }
    }
}
